package AmonTen::Model::StockQuote;
use strict;
use warnings;

use Finance::Quote;
use Moose;

has quote => (
    is      => 'ro',
    isa     => 'Finance::Quote',
    default => sub { Finance::Quote->new("Yahoo::Asia") },
    handles => ['fetch']
);

1;

__END__
