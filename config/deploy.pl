#!/usr/bin/env perl

use warnings;
use strict;

use Cinnamon::DSL;

my $app = 'AmonTen';

my $eval_plenv
    = q#PATH=~/.plenv/bin:$PATH; eval "$(~/.plenv/bin/plenv init -)"#;

my $service_dir = q#$HOME/service#;
my $service     = "$service_dir/$app";

my $aws_config = "config/aws.pl";

set user => 'zakame';

set application => $app;
set repository  => "https://bitbucket.org/zakame/$app.git";

my $devhost = $ENV{DEPLOY_HOST};

role development => $devhost => {
    deploy_to => "/home/zakame/app/$app",
    branch    => "master",
};

task deploy => {
    setup => sub {
        my ($host) = @_;
        my $repo   = get 'repository';
        my $dep    = get 'deploy_to';
        my $branch = 'origin/' . get 'branch';

        remote {
            run "git clone $repo $dep && cd $dep && git checkout -q $branch";
            run "$eval_plenv && cd $dep && plenv local 5.14.2";
        }
        $host;
    },
    secret => sub {
        my ($host) = @_;
        my $dep = get 'deploy_to';

        run "scp", $aws_config, "$host:$dep/$aws_config";
    },
    update => sub {
        my ($host) = @_;
        my $dep    = get 'deploy_to';
        my $branch = 'origin/' . get 'branch';

        remote {
            run "cd $dep && git fetch origin && git checkout -q $branch";
        }
        $host;
    },
};

# assume that $app is already started via daemontools here
# e.g. via svscan $service_dir &
task server => {
    start => sub {
        my ($host) = @_;
        remote {
            run "svc -u $service";
        }
        $host;
    },
    stop => sub {
        my ($host) = @_;
        remote {
            run "svc -d $service";
        }
        $host;
    },
    restart => sub {
        my ($host) = @_;
        call 'server:stop',  $host;
        call 'server:start', $host;
    },
    status => sub {
        my ($host) = @_;
        remote {
            run "svstat $service";
        }
        $host;
    },
};

task carton => {
    install => sub {
        my ($host) = @_;
        my $dep = get 'deploy_to';

        remote {
            run "$eval_plenv && cd $dep && carton install";
        }
        $host;
    },
};

__END__
