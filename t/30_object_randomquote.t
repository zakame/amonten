use strict;
use warnings;

use Test::More;

use AmonTen::Model::RandomQuote;

my $RandomQuote = AmonTen::Model::RandomQuote->new();

ok( $RandomQuote->get_one() );
ok( $RandomQuote->get_all() );

done_testing;