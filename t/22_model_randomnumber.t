use strict;
use warnings;
use Test::More tests => 1;

use AmonTen::Model::RandomNumber;

my $m = AmonTen::Model::RandomNumber->new;

subtest 'basics' => sub {
    plan tests => 3;
    
    isa_ok $m, 'AmonTen::Model::RandomNumber';
    can_ok $m, 'generate';

    subtest 'basic generate' => sub {
        plan tests => 2;

        ok $m->generate, 'got a random value';
        like $m->generate, qr/\d+/, 'got a random number';
    };
};
